
import 'package:HirohataCargo/ui/screens/edit/drag/dragProvider.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:HirohataCargo/ui/screens/edit/table/recordProvider.dart';
import 'package:HirohataCargo/ui/screens/order/orderProvider.dart';
import 'package:HirohataCargo/ui/screens/splash/loginProvider.dart';
import 'package:HirohataCargo/ui/screens/view/viewScreenProvider.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;

void setup() {
  getIt.registerSingleton<LoginProvider>(LoginProvider());
  getIt.registerSingleton<OrderProvider>(OrderProvider());
  getIt.registerSingleton<ViewProvider>(ViewProvider());
  getIt.registerSingleton<DragProvider>(DragProvider());
  getIt.registerSingleton<DrawProvider>(DrawProvider());
  getIt.registerSingleton<RecordProvider>(RecordProvider());
}
