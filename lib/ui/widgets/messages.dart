import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:flutter/material.dart';

dialogMessage(BuildContext context, String msg) {
  return Dialog(
//            contentPadding: EdgeInsets.all(5),
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      // height: 134,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: <Widget>[
          //     Text(
          //       'Messages',
          //       style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
          //     ),
          //     IconButton(
          //       icon: Icon(Icons.clear, color: Colors.purple,),
          //       onPressed: () {
          //         Navigator.pop(context);
          //       },
          //     )
          //   ],
          // ),
          SizedBox(
            height: 20,
            width: 1,
          ),
          Center(child: Text("${msg}")),
          SizedBox(
            height: 30,
            width: 1,
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              InkWell(
                child: Container(
                    child: Text(
                  Button.btnOk,
                  style: TextStyle(
                      color: Colors.purple,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                )),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              SizedBox(
                height: 1,
                width: 20,
              ),
            ],
          )
        ],
      ),
    ),
  );
}

dialogConfirm(BuildContext context, String msg) {
  return Dialog(
//            contentPadding: EdgeInsets.all(5),
    elevation: 0.0,
    backgroundColor: Colors.transparent,
    child: Container(
      padding: EdgeInsets.all(10),
      color: Colors.white,
      width: 420,
      // height: 160,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            height: 20,
            width: 1,
          ),
          Center(child: Text("${msg}")),
          SizedBox(
            height: 30,
            width: 1,
          ),
          Divider(),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              // RaisedButton(
              //   color: Colors.transparent,
              //   onPressed: () {
              //     Navigator.pop(context, 1);
              //   },
              //   child: Text('Ok', style: TextStyle(color: Colors.white)),
              // ),
              InkWell(
                child: Container(
                    // height: 20,
                    // width: 55,
                    child: Center(
                  child: Text(
                    Button.btnCancel,
                    style: TextStyle(
                        color: Colors.purple,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                )),
                onTap: () {
                  Navigator.pop(context, 0);
                },
              ),
              SizedBox(
                height: 1,
                width: 30,
              ),
              InkWell(
                child: Container(
                    // height: 20,
                    // width: 55,
                    child: Text(
                  Button.btnOk,
                  style: TextStyle(
                      color: Colors.purple,
                      fontWeight: FontWeight.bold,
                      fontSize: 16),
                )),
                onTap: () {
                  Navigator.pop(context, 1);
                },
              ),
              SizedBox(
                height: 1,
                width: 20,
              ),
            ],
          )
        ],
      ),
    ),
  );
}
