import 'dart:async';
import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart';

class LoginApi {
  Future<Either<Exception, Response>> login(String user, String password) async {
    user = user.trim();
    password = password.trim();
    try {
      final url =
          NetworkUtil.BASE_URL + '/api/login?userCode=$user&password=$password';
      Response response = await post(url).timeout(
        Duration(seconds: 10),
      );
      return Right(response);
    } catch (e) {
      return Left(e);
    }
  }
}
