class Box {
  dynamic height;
  dynamic width;
  double posX;
  double posY;

  Box(height, width, posX, posY) {
    this.height = height;
    this.width = width;
    this.posX = posX;
    this.posY = posY;
  }

  @override
  String toString() {
    // TODO: implement toString
    return "${this.posX}  ${this.posY}";
  }
}
