import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:http/http.dart';

class UploadFileApi {
  Future<String> uploadPdf(filename) async {
    String url = NetworkUtil.BASE_URL + "/api/pdf/upload";
    var request = MultipartRequest('POST', Uri.parse(url));

    request.files.add(await MultipartFile.fromPath('file', filename));
    request.headers.addAll(NetworkUtil.getRequestHeaders());
    var res = await request.send();
    // String str = await res.stream.bytesToString();
    return "str";
  }
}
