import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/src/utils/AppConfig.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/DrawInput.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawButton.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:HirohataCargo/ui/widgets/loading.dart';
import 'package:after_layout/after_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class DrawWidget extends StatefulWidget {
  final String order;
  bool hasTwoType;

  DrawWidget({this.order, this.hasTwoType});
  @override
  _DrawWidgetState createState() => new _DrawWidgetState();
}

class _DrawWidgetState extends State<DrawWidget> with AfterLayoutMixin {
  GlobalKey _globalKey = new GlobalKey();
  final provider = getIt<DrawProvider>();

  var encoded;
  var text = "";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    provider.getInfor(widget.order).then((value) {
      provider.getNotes();
    });
  }

  @override
  Widget build(BuildContext context) {
    // final dataModel = Provider.of<DrawPadBloc>(context);
    // dataModel.globalKey = _globalKey;

    return Stack(
      children: [
        RepaintBoundary(
          key: _globalKey,
          child: Container(
            // color: Color(0xffFFFAF8),
            child: new GestureDetector(
                onPanStart: (detail) {
                  provider.startPoint = provider.points.length;
                },
                onPanCancel: () {},
                onPanUpdate: (DragUpdateDetails details) {
                  provider.onPanUpdate(details);
                },
                onPanEnd: (DragEndDetails details) async {
                  provider.onPanEnd();

                  // img.Image signatureImage = img.Image(
                  //     (Config.size.width / 13 * 4).toInt(),
                  //     (Config.size.height / 6 * 4).toInt());
                },
                child: getDrawStream()),
          ),
        ),
        getStream(),
        DrawButton(),

        // Container(
        //   child: iimage,
        // ),
      ],
    );
  }

  Widget getDrawStream() {
    return StreamBuilder<List<Offset>>(
        stream: provider.streamPoints,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return new CustomPaint(
              painter: new Signature(points: snapshot.data),
              size: Size.infinite,
            );
          } else {
            return innerLoading();
          }
        });
  }

  Widget getStream() {
    return StreamBuilder<Infor>(
        stream: provider.streamInfor,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return DrawInput(infor: snapshot.data, hasTwoType: widget.hasTwoType,);
          } else {
            return innerLoading();
          }
        });
  }

  @override
  void afterFirstLayout(BuildContext context) {
    // TODO: implement afterFirstLayout
    provider.globalKey = _globalKey;
  }
}

class Signature extends CustomPainter {
  List<Offset> points;

  Signature({this.points});

  @override
  Future<void> paint(Canvas canvas, Size size) async {
    Paint paint = new Paint()
      ..color = Colors.black
      ..strokeCap = StrokeCap.round
      ..strokeWidth = 1.5;

    for (int i = 0; i < points.length - 1; i++) {
      if (points[i] != null && points[i + 1] != null) {
        canvas.drawLine(points[i], points[i + 1], paint);
      }
    }
    // final recorder = new PictureRecorder();
    // final picture = recorder.endRecording();
    // final img = await picture.toImage(200, 200);
    // final pngBytes = await img.toByteData(format: ImageByteFormat.png);
  }

  @override
  bool shouldRepaint(Signature oldDelegate) => oldDelegate.points != points;
}
