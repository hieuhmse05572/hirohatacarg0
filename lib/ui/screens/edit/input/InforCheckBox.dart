import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/ui/screens/edit/drag/dragProvider.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:HirohataCargo/ui/widgets/dialog.dart';
import 'package:flutter/material.dart';

class InforCheckBox extends StatefulWidget {
  final Infor infor;
  InforCheckBox({this.infor});
  @override
  _InforCheckBoxState createState() => _InforCheckBoxState();
}

class _InforCheckBoxState extends State<InforCheckBox> {
  final drag = getIt<DragProvider>();
  final provider = getIt<DrawProvider>();

  @override
  Widget build(BuildContext context) {
    int type = provider.type;
    bool topVal = widget.infor == null ? false : widget.infor.sutashon;
    bool botVal = widget.infor == null ? false : widget.infor.hakka_oroshi;
    return Padding(
      padding: EdgeInsets.only(left: 10, right: 20),
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            height: 25,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Checkbox(
                  onChanged: (bool value) {
                    setState(() {
                      if(type == 1) type = 0;
                      else type = 1;
                    });
                    provider.type = type;
                    drag.updateWidget();
                  },
                  value: type == 1 ? true: false,
                ),
                Container(width: 50, child: Text("カド当 ")),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            height: 25,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Checkbox(
                  onChanged: (bool value) {
                    setState(() {
                      if(type == 2) type = 0;
                      else type = 2;
                    });
                    provider.type = type;
                    drag.updateWidget();

                  },
                  value: type == 2 ? true: false,
                ),
                Container(width: 50, child: Text("ゴム ")),
              ],
            ),
          ),
          SizedBox(height: 10, width: 1,),
          Container(
            height: 25,
            alignment: Alignment.centerLeft,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Checkbox(
                  onChanged: (bool value) {
                    setState(() {
                      topVal = value;
                    });
                    if (value)
                      provider.updateCheckBox(1, true);
                    else
                      provider.updateCheckBox(1, false);
                  },
                  value: topVal,
                ),
                Container(width: 50, child: Text("ｽﾀﾝｼｮﾝ ")),
              ],
            ),
          ),
          Container(
            alignment: Alignment.centerLeft,
            height: 25,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Checkbox(
                  onChanged: (bool value) {
                    setState(() {
                      botVal = value;
                    });
                    if (value)
                      provider.updateCheckBox(2, true);
                    else
                      provider.updateCheckBox(2, false);
                  },
                  value: botVal,
                ),
                Container(width: 50, child: Text("ﾊｯｶｰ卸 ")),
              ],
            ),
          ),
          SizedBox(height: 20, width: 1,),
          Divider(),
          SizedBox(height: 10, width: 1,),

          // Container(
          //   alignment: Alignment.bottomLeft,
          //     child: Text('注意書き: ', style: TextStyle(fontSize: 12),)),

        ],
      ),
    );
  }
}
