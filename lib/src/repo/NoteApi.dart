import 'dart:async';
import 'package:HirohataCargo/src/utils/NetworkUtil.dart';
import 'package:dartz/dartz.dart';
import 'package:http/http.dart';

class NoteApi {
  Future<Either<Exception, Response>> getNotes() async {
    try {
      final url = NetworkUtil.BASE_URL + '/api/getNotes';
      Response response = await post(url, headers: NetworkUtil.getRequestHeaders()).timeout(
        Duration(seconds: 10),
      );
      return Right(response);
    } catch (e) {
      return Left(e);
    }
  }
}
