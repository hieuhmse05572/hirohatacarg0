import 'dart:math';

import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/src/utils/utils.dart';

class Metal {
  int id;
  int type;
  int rotation;
  String productCode;
  int rownumber;
  int columnNumber;
  dynamic height;
  dynamic width;
  dynamic length;
  double posX;
  double posY;
  dynamic corners;
  List<int> pads = [];

  // Metal(id, type, rotation, productCode, height, width, length, posX, posY,
  //     corners) {
  //   this.id = id;
  //   this.type = type;
  //   this.rotation = rotation;
  //   this.productCode = productCode;
  //   this.posX = posX * Configs.scale;
  //   this.height = height * Configs.scale;
  //   this.posY = 300 - posY * Configs.scale - this.height;
  //   this.length = width * Configs.scale;
  //   this.width = length * Configs.scale;
  //   this.corners = [];
  factory Metal.scaleTo(Metal metal, double scale) => Metal(
      metal.id,
      metal.type,
      metal.rotation,
      metal.productCode,
      metal.height * scale,
      metal.length * scale,
      metal.width * scale,
      metal.posX * scale,
      300 - metal.posY * scale - metal.height * scale,
      metal.corners,
      metal.pads,
      metal.rownumber,
      metal.columnNumber);

  factory Metal.cloneToEdit(Metal metal, double scale) => Metal(
      metal.id,
      metal.type,
      metal.rotation,
      metal.productCode,
      metal.height * scale,
      metal.length * scale,
      metal.width * scale,
      metal.posX * scale,
      Configs.baseLine - metal.posY * scale - metal.height * scale,
      metal.corners,
      metal.pads,
      metal.rownumber,
      metal.columnNumber);
  factory Metal.revert(Metal metal, double scale, double minLeft) => Metal(
      metal.id,
      metal.type,
      metal.rotation,
      metal.productCode,
      metal.height / scale,
      metal.length / scale,
      metal.width / scale,
      (metal.posX - minLeft) / scale,
      (Configs.baseLine - metal.posY - metal.height) / scale,
      metal.corners,
      metal.pads,
      metal.rownumber,
      metal.columnNumber);

  Metal(id, type, rotation, productCode, height, width, length, posX, posY,
      corners, pads, rownumber, columnNumber) {
    this.id = id;
    this.type = type;
    this.rotation = rotation;
    this.productCode = productCode;
    this.posX = posX * 1.0;
    this.height = height * 1.0;
    this.posY = posY * 1.0;
    this.length = width * 1.0;
    this.width = length * 1.0;
    this.corners = [];
    // this.pads = pads;
    // List<Point> listPad = [];
    this.pads = [];
    if (pads != [])
      for (var i in pads) {
        int pad = i as int;
        this.pads.add(pad);
      }
    this.rownumber = rownumber;
    this.columnNumber = columnNumber;
    // print("corners");
  }

  factory Metal.clone(Metal metal) => Metal(
      metal.id,
      metal.type,
      metal.rotation,
      metal.productCode,
      metal.height,
      metal.length,
      metal.width,
      metal.posX,
      metal.posY,
      metal.corners,
      metal.pads,
      metal.rownumber,
      metal.columnNumber);

  // get the four side coordinates of the rectangle
  double get bottom {
    return this.posY * 1.0 + this.height;
  }

  double get left {
    return this.posX * 1.0;
  }

  double get right {
    return this.posX + this.width;
  }

  double get top {
    return this.posY * 1.0;
  }

  isInner(double x, double y) {
    if (x > left && x < right && y > top && y < bottom) {
      return true;
    }
    return false;
  }

  // getSideOfMetal(Metal metal){
  //   if(this.top < metal.top && this.)
  //
  // }

  checkCollision(Metal metal) {
    if (metal.posX == this.posX && metal.posY == posY) return false;
    bool collisionX = this.right - 1 >= metal.posX && metal.right >= this.posX;
    bool collisionY = this.bottom >= metal.posY && metal.bottom >= this.posY;
    return collisionX && collisionY;
  }

  factory Metal.fromJson(Map<String, dynamic> json) {
    if (json['type'] as int == 3) return null;
    // return Metal(
    //     json["id"],
    //     json["type"] as int,
    //     json["rotation"],
    //     json["productCode"],
    //     0,
    //     0,
    //     0,
    //     json["posX"] as double,
    //     json["posY"] as double,
    //     json["corners"]);
    return Metal(
      json["id"],
      json["type"] as int,
      json["rotation"],
      json["productCode"],
      json["height"][0],
      json["width"][0],
      json["length"][0],
      json["posX"] as double,
      json["posY"] as double,
      [],
      json["pads"],
      json["row_number"],
      json["column_number"],
    );
  }
  factory Metal.fromJson2(Map<String, dynamic> json) {
    if (json['type'] as int == 3) return null;
    // return Metal(
    //     json["id"],
    //     json["type"] as int,
    //     json["rotation"],
    //     json["productCode"],
    //     0,
    //     0,
    //     0,
    //     json["posX"] as double,
    //     json["posY"] as double,
    //     json["corners"]);
    return Metal(
      json["id"],
      json["type"] as int,
      json["rotation"],
      json["productCode"],
      json["heights"][0],
      json["widths"][0],
      json["lengths"][0],
      json["posX"] as double,
      json["posY"] as double,
      [],
      json["pads"],
      json["row_number"],
      json["column_number"],
    );
  }
  padsToString() {
    String str = "";
    this.pads.forEach((element) {
      str += "${element},";
    });
    str = Util.removeLastCharacter(str);
    return str;
  }

  String toJson() {
    corners = [];
    String list = "";
    if (type == 3)
      list +=
          '\{ "id":$id, "type": $type , "rotation": $rotation , "productCode": "$productCode" ,"corners": \[  \] , "posX": $posX ,  "posY": $posY   \}';
    else
      list +=
          '\{ "id":$id, "type": $type , "rotation": $rotation ,"pads":\[ ${padsToString()} \] , "row_number": "$rownumber", "column_number": "$columnNumber","productCode": "$productCode" , "height": \[ $height \] , "width" : \[ $width \], "length" :  \[ $length \], "posX": $posX ,  "posY": $posY   \}';
    return list;
  }
  //
  // String tojson() {
  //   products.forEach(mapEntry);
  //   return "[ " + list + "]";
  // }
}
