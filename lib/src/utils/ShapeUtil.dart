import 'package:HirohataCargo/src/models/metal.dart';

class ShapeUtil {
  static getMaxRowNumber(List<Metal> metals) {
    if (metals != null && metals.isNotEmpty) {
      int max = metals[0].rownumber;
      metals.forEach((element) {
        if (element.rownumber > max) max = element.rownumber;
      });
      return max;
    }
    return 0;
  }
}
