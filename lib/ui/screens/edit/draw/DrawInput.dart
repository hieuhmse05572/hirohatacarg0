import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:flutter/material.dart';

class DrawInput extends StatelessWidget {
  Infor infor;
  bool hasTwoType;
  DrawInput({this.infor, this.hasTwoType});
  @override
  Widget build(BuildContext context) {
    // infor = Infor("hjhjh", "sdsd", "", false, "", "", false);
    return Container(
      child: Stack(
        children: [
          Container(
            child: TextFormField(
              onChanged: (value) {
                infor.note = value;
                // String str = Util.removeLastCharacter(dataModel.drawing);
                // dataModel.drawing = str + " ";
                // textEditingController.text = value;
              },
              initialValue: infor == null ? "" : infor.note,
              // controller: textEditingController,
              decoration: const InputDecoration(
                counterText: "",
                // suffixIcon: Container(),
                // border: Border(),
                contentPadding: EdgeInsets.all(15), //here your padding
//              fillColor: Colors.white
              ),
              // keyboardType: TextInputType.multiline,
              maxLines: 7,
              maxLength: 150,
            ),
          ),
          Positioned(
            bottom: 0,
            left: 30,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                infor.note_checkbox_str != null
                    ? Text("${infor.note_checkbox_str.replaceAll("[", "").replaceFirst("]", "")}",
                    style: TextStyle(fontSize: 14))
                    : Container(),
                infor.sutashon
                    ? Text(Constants.sutansho, style: TextStyle(fontSize: 14))
                    : Container(),
                infor.hakka_oroshi
                    ? Text(Constants.hakka_oroshi,
                        style: TextStyle(fontSize: 14))
                    : Container(),
                infor.manbo != ""
                    ? Text("マンボ ${infor.manbo}  本",
                        style: TextStyle(fontSize: 14))
                    : Container(),
                infor.kobaku != ""
                    ? Text("固縛 ${infor.kobaku}  点",
                        style: TextStyle(fontSize: 14))
                    : Container(),
                hasTwoType
                    ? Text(Constants.hasBoth, style: TextStyle(fontSize: 14))
                    : Container(),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
