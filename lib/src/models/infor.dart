class Infor {
  // String checkbox1;
  // String checkbox2;
  // String text1;
  // String text2;

  String productCode;
  String note;
  String drawing;
  bool sutashon;
  String manbo;
  String kobaku;
  bool hakka_oroshi;
  String note_checkbox;
  String note_checkbox_str;

  // Infor(check1, check2, text1, text2) {
  //   this.checkbox1 = check1;
  //   this.checkbox2 = check2;
  //   this.text1 = text1;
  //   this.text2 = text2;
  // }

  factory Infor.clone(Infor infor) => Infor(
      infor.productCode,
      infor.note,
      infor.drawing,
      infor.sutashon,
      infor.manbo,
      infor.kobaku,
      infor.hakka_oroshi,
      infor.note_checkbox);

  compareTo(Infor infor) {
    if (infor.sutashon == this.sutashon &&
        infor.hakka_oroshi == this.hakka_oroshi &&
        infor.kobaku == this.kobaku &&
        infor.manbo == this.manbo &&
        infor.note_checkbox == this.note_checkbox &&
        // this.drawing == infor.drawing &&
        this.note == infor.note) return true;
    return false;
  }

  Infor(productCode, note, drawing, sutashon, manbo, kobaku, hakka_orishi,
      note_checkbox) {
    this.productCode = productCode;
    this.note = note;
    this.drawing = drawing;
    this.sutashon = sutashon;
    this.manbo = manbo;
    this.kobaku = kobaku;
    this.hakka_oroshi = hakka_orishi;
    this.note_checkbox = note_checkbox;
  }
  factory Infor.fromJson(Map<String, dynamic> json) {
    return Infor(
      json["productCode"],
      json["note"] == null ? "" : json["note"],
      json["drawing"] == null ? "" : json["drawing"],
      json["sutansho"] == null ? false : json["sutansho"] as bool,
      json["manbo"] == null ? "" : json["manbo"],
      json["kobaku"] == null ? "" : json["kobaku"],
      json["hakka_oroshi"] == null ? false : json["hakka_oroshi"] as bool,
      json["noteCheckBox"] == null ? "" : json["noteCheckBox"],
    );
  }
  String toJson() {
    String list = "";
    list +=
        '\{ "productCode": "$productCode", "note": "$note" , "drawing": "$drawing" , "sutansho": $sutashon , "manbo": "$manbo" ,  "kobaku": "$kobaku"  ,  "hakka_oroshi": $hakka_oroshi  ,  "noteCheckBox": "$note_checkbox"\}';
    return list;
  }
}
