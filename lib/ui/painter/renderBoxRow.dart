import 'package:HirohataCargo/src/utils/colors.dart';
import 'package:HirohataCargo/ui/painter/renderText.dart';
import 'package:flutter/material.dart';

class BoxWithText extends CustomPainter {
  final Offset position;
  final double text;
  BoxWithText({this.text, this.position});

  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..strokeWidth = 1
      ..color = Colors.black
      ..style = PaintingStyle.stroke;
    double height = 20;
    double width = 40;
    double fontsize = 9;

    Size sizeOfText = getSizeOfText("${text }", TextStyle(fontSize: fontsize));
    width = sizeOfText.width+ 6;
    Rect rect = Rect.fromLTWH(position.dx, position.dy - height, width, height);
    // canvas.drawRect(rect, paint);
    TextUtil.drawTextCenter(canvas,text *1000 , rect.center.dx - sizeOfText.width / 2,
        rect.center.dy - sizeOfText.height / 2, fontsize);
  }

  Size getSizeOfText(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class DrawText extends CustomPainter {
  final Offset position;
  final String text;
  DrawText({this.text, this.position});

  @override
  void paint(Canvas canvas, Size size) {

    double height = 20;
    double width = 40;
    double fontsize = 20;

    Size sizeOfText = getSizeOfText("${text}", TextStyle(fontSize: fontsize));
    width = sizeOfText.width+ 6;
    Rect rect = Rect.fromLTWH(position.dx, position.dy - height, width, height);
    TextUtil.drawTextStyle(canvas,text, rect.center.dx - sizeOfText.width / 2,
        rect.center.dy - sizeOfText.height / 2, fontsize);
  }

  Size getSizeOfText(String text, TextStyle style) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: style),
        maxLines: 1,
        textDirection: TextDirection.ltr)
      ..layout(minWidth: 0, maxWidth: double.infinity);
    return textPainter.size;
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
