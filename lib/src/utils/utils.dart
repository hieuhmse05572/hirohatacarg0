import 'dart:math';

import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/models/record.dart';
import 'package:HirohataCargo/ui/widgets/messages.dart';
import 'package:flutter/material.dart';

import '../models/truck.dart';
import 'constant.dart';

class Util {
  static Metal getTopBox(List<Metal> metals) {
    Metal metal = metals[0];
    metals.forEach((element) {
      if (element.posY <= metal.posY) {
        metal = element;
      }
    });
    return metal;
  }

  static Truck chooseBestTruck(List<Metal> metals) {
    Point minMaxOfLength = getMinLength(metals);
    double widthOfBloc = getWidthOfBloc(metals);
    if (minMaxOfLength.y <= Constants.truck4t.length &&
        widthOfBloc <= Constants.truck4t.width)
      return Constants.truck4t;
    else if (minMaxOfLength.y <= Constants.truck10t.width &&
        widthOfBloc <= Constants.truck10t.width)
      return Constants.truck10t;
    else
      return Constants.truckContainer;
  }

  static bool checkTruck(List<Metal> metals, Truck truck) {
    Point minMaxOfLength = getMinLength(metals);
    double widthOfBloc = getWidthOfBloc(metals);
    if (minMaxOfLength.y <= truck.length && widthOfBloc <= truck.width) {
      return true;
    }
    return false;
  }

  static Point getMinLength(List<Metal> metals) {
    double min = metals[0].length;
    double max = metals[0].length;
    metals.forEach((element) {
      if (element.length < min) min = element.length;
      if (element.length > max) max = element.length;
    });
    return Point(min, max);
  }

  static double getWidthOfBloc(List<Metal> metals) {
    double max = metals[0].right;
    metals.forEach((element) {
      if (element.right > max) max = element.right;
    });
    return max;
  }

  static double getMinOfBloc(List<Metal> metals) {
    double min = metals[0].left;
    metals.forEach((element) {
      if (element.left <= min) min = element.left;
    });
    return min;
  }

  static Point getMinMaxOfWidth(List<Metal> metals) {
    double min = metals[0].width;
    double max = metals[0].width;
    metals.forEach((element) {
      if (element.width < min) min = element.width;
      if (element.width > max) max = element.width;
    });

    return Point(min, max);
  }

  static Point getTopLeftPoint(List<Metal> metals, Metal topBox) {
    Point point = Point(topBox.posX, topBox.posY);
    metals.forEach((element) {
      if (element.posY.floor() == point.y.floor() && element.posX <= point.x) {
        point = Point(element.posX, element.posY);
      }
    });
    return point;
  }

  static Point getTopRightPoint(List<Metal> metals, Metal topBox) {
    Metal metal;
    Point point = Point(topBox.posX, topBox.posY);
    metals.forEach((element) {
      if (element.posY.floor() == point.y.floor() && element.posX >= point.x) {
        point = Point(element.posX, element.posY);
        metal = element;
      }
    });
    point = Point(point.x + metal.width, point.y);
    return point;
  }

  static double getLineBase(List<Metal> metals) {
    double max = metals[0].posY + metals[0].height;
    metals.forEach((element) {
      if (max < element.posY + element.height) {
        max = element.posY + element.height;
      }
    });
    return max;
  }

  static String removeLastCharacter(String str) {
    String result = "";
    if ((str != null) && (str.length > 0)) {
      result = str.substring(0, str.length - 1);
    }

    return result;
  }

  static Point getBottomRightPoint(List<Metal> metals, double bottom) {
    double max = metals[0].right;
    metals.forEach((element) {
      if (element.right > max) {
        max = element.right;
      }
    });
    return Point(max + 5.0, bottom);
  }

  static Point getBottomLeftPoint(List<Metal> metals, double bottom) {
    double min = metals[0].left;
    metals.forEach((element) {
      if (element.left < min) {
        min = element.left;
      }
    });
    return Point(min - 5.0, bottom);
  }

  static checkDuplicate(Record record1, Record record2) {
    if (record1.length == record2.length &&
        record1.height == record2.height &&
        record1.width == record2.width &&
        record1.anointingClassification == record2.anointingClassification &&
        record1.row_number == record2.row_number &&
        record2.number_of_individuals == record1.number_of_individuals &&
        record1.contract_number == record2.contract_number) return true;
    return false;
  }

  static sortMetalDesc(List<Metal> metals) {
    // sắp xếp giảm Y
    metals.sort((a, b) {
      if (a.posY > b.posY)
        return -1; //1 tang dan ; -1 giam dan
      else if (a.posY < b.posY)
        return 1;
      else
        return 0;
    });
  }

  static calRowNumber(List<Metal> metals) {
    // metals.sort((a, b) {
    //   if (a.bottom > b.bottom)
    //     return -1; //1 tang dan ; -1 giam dan
    //   else if (a.bottom < b.bottom)
    //     return 1;
    //   else
    //     return 0;
    // });
    int rowNumber = 1;
    double bottom = metals[0].bottom;
    metals.forEach((element) {
      if (element.type != 3) {
        if (element.bottom.round() < bottom.round()) {
          rowNumber++;
          bottom = element.bottom;
        }
        if ((element.bottom.round() == bottom.round())) {
          element.rownumber = rowNumber;
        }
      }
    });
  }
}
