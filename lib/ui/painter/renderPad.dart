import 'dart:math';

import 'package:HirohataCargo/src/models/metal.dart';
import 'package:flutter/material.dart';

class PadPainter extends CustomPainter {
  final Metal metal;
  int rotation = 1;
  PadPainter({this.metal, this.rotation});
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.teal
      ..strokeWidth = 5
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    Path path = new Path();
    // 1 top left
    // 2 top right
    // 3 bottom right
    // 4 bottom left

    if (rotation == 1) {
      double posX = metal.left - 4;
      double posY = metal.top - 4;
      path.moveTo(posX, posY);
      path.lineTo(posX + 30, posY);
      path.moveTo(posX, posY);
      path.lineTo(posX, posY + 30);
      canvas.drawPath(path, paint);
    } else if (rotation == 2) {
      double posX = metal.right + 4;
      double posY = metal.top - 4;
      path.moveTo(posX, posY);
      path.lineTo(posX - 30, posY);
      path.moveTo(posX, posY);
      path.lineTo(posX, posY + 30);
      canvas.drawPath(path, paint);
    } else if (rotation == 3) {
      double posX = metal.right + 4;
      double posY = metal.bottom + 4;
      path.moveTo(posX, posY);
      path.lineTo(posX - 30, posY);
      path.moveTo(posX, posY);
      path.lineTo(posX, posY - 30);
      canvas.drawPath(path, paint);
    } else {
      double posX = metal.left - 4;
      double posY = metal.top + 4;
      path.moveTo(posX, posY);
      path.lineTo(posX + 30, posY);
      path.moveTo(posX, posY);
      path.lineTo(posX, posY - 30);
      canvas.drawPath(path, paint);
    }

    // canvas.drawLine(startingPoint, midPoint, paint);
    // canvas.drawLine(midPoint, endingPoint, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class PadTopLeftPainter extends CustomPainter {
  final Point topLeft;
  final double scale;
  PadTopLeftPainter(this.topLeft, this.scale);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.teal
      ..strokeWidth = 3
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    Path path = new Path();
    // 1 top left
    // 2 top right
    // 3 bottom right
    // 4 bottom left

    double posX = topLeft.x * scale - 2 ;
    double posY = topLeft.y * scale - 2 ;
    path.moveTo(posX, posY);
    path.lineTo(posX + 10, posY);
    path.moveTo(posX, posY);
    path.lineTo(posX, posY + 10);
    canvas.drawPath(path, paint);

    // canvas.drawLine(startingPoint, midPoint, paint);
    // canvas.drawLine(midPoint, endingPoint, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}

class PadTopRightPainter extends CustomPainter {
  final Point topRight;
  final double scale;
  PadTopRightPainter(this.topRight, this.scale);
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.teal
      ..strokeWidth = 3
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke;
    Path path = new Path();
    // 1 top left
    // 2 top right
    // 3 bottom right
    // 4 bottom left

    double posX = topRight.x * scale + 2 ;
    double posY = topRight.y * scale - 2 ;
    path.moveTo(posX, posY);
    path.lineTo(posX - 10, posY);
    path.moveTo(posX, posY);
    path.lineTo(posX, posY + 10);
    canvas.drawPath(path, paint);

    // canvas.drawLine(startingPoint, midPoint, paint);
    // canvas.drawLine(midPoint, endingPoint, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
