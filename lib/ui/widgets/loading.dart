import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget innerLoading() {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CupertinoActivityIndicator(
          radius: 14,
        ),
        SizedBox(
          height: 6,
          width: 1,
        ),
        Container(alignment: Alignment.center, child: Text('ローディング...'))
      ],
    ),
  );
}

Widget Loading() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      Center(
        child: Container(
            height: 100,
            width: 100,
            decoration: new BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(7)),
              boxShadow: [
                BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    blurRadius: 4,
                    offset: Offset(1, 1))
              ],
              color: Colors.grey,
            ),
            margin: EdgeInsets.all(20),
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CupertinoActivityIndicator(
                  radius: 14,
                ),
                SizedBox(
                  height: 6,
                  width: 1,
                ),
                Text(
                  'ローディング',
                  style: TextStyle(color: Colors.white),
                )
              ],
            ))),
      ),
    ],
  );
}

Dialog dialogLoading() {
  return Dialog(
      elevation: 0, backgroundColor: Colors.transparent, child: Loading());
}
