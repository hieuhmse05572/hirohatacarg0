import 'package:HirohataCargo/ui/screens/splash/splash.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Cargo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primaryColor: Colors.lightBlue,
        primarySwatch: Colors.lightBlue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Splash(),
      // initialRoute: '/splash',
      // routes: {
      //   // When navigating to the "/" route, build the FirstScreen widget.
      //   // '/login': (context) => Login(),
      //   '/splash': (context) => Splash(),
      //   // When navigating to the "/second" route, build the SecondScreen widget.
      //   '/orders': (context) => OrderScreen(),
      //   '/view': (context) => ViewScreen(),
      //   '/edit': (context) => EditScreen(),
      // },
    );
  }
}
