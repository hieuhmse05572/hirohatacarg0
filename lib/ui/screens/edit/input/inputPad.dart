import 'dart:io';
import 'package:intl/intl.dart';

import 'package:HirohataCargo/getIt.dart';
import 'package:HirohataCargo/src/helper/uploadFileHelper.dart';
import 'package:HirohataCargo/src/models/infor.dart';
import 'package:HirohataCargo/src/models/metal.dart';
import 'package:HirohataCargo/src/models/record.dart';
import 'package:HirohataCargo/src/utils/AppConfig.dart';
import 'package:HirohataCargo/src/utils/constant.dart';
import 'package:HirohataCargo/ui/screens/edit/drag/dragProvider.dart';
import 'package:HirohataCargo/ui/screens/edit/draw/drawProvider.dart';
import 'package:HirohataCargo/ui/screens/edit/table/recordProvider.dart';
import 'package:HirohataCargo/ui/screens/edit/table/tablePdf.dart';
import 'package:HirohataCargo/ui/screens/view/view_screen2.dart';
import 'package:HirohataCargo/ui/widgets/dialog.dart';
import 'package:HirohataCargo/ui/widgets/loading.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:printing/printing.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:toast/toast.dart';

import 'InforCheckBox.dart';
import 'InforInput.dart';

class InputPad extends StatefulWidget {
  final String order;
  final bool hasBothType;
  InputPad({this.order, this.hasBothType});
  @override
  _InputPadState createState() => _InputPadState();
}

class _InputPadState extends State<InputPad> {
  final drawProvider = getIt<DrawProvider>();
  final dragProvider = getIt<DragProvider>();
  final recordProvider = getIt<RecordProvider>();

  @override
  Widget build(BuildContext context) {
    return DottedBorder(
      color: Colors.grey,
      strokeWidth: 2,
      child: Container(
        color: Color(0xffFFFAF8),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Column(
              children: [
                getStream(),
                _getAddVerticalWoodButtonStream(),
                _getRightMenu()
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget getStream() {
    return StreamBuilder<Infor>(
        stream: drawProvider.streamInfor,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Widget> widgets = [];
            widgets.add(InforCheckBox(infor: snapshot.data));
            widgets.add(InforInput(infor: snapshot.data));
            return Column(
              mainAxisSize: MainAxisSize.min,
              children: widgets,
            );
          } else {
            return Container();
          }
        });
  }

  Widget _getAddVerticalWoodButtonStream() {
    return StreamBuilder<Metal>(
        stream: dragProvider.streamSelectedShape,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return _getAddVerticalWoodButton();
          } else {
            return Container();
          }
        });
  }

  Widget _getAddVerticalWoodButton() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        true
            ? Text(
                Constants.verticalWood,
                style: TextStyle(fontSize: 15),
              )
            : Container(),
        SizedBox(
          height: 10,
          width: 1,
        ),
        Divider(),
        true
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  InkWell(
                    onTap: () {
                      dragProvider.addVeritcalWood(4);
                    },
                    child: Container(
                      decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ],
                          color: Colors.grey.withOpacity(0.1),
                          border: new Border.all(
                            color: Colors.blue.withOpacity(0.3),
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      width: 40,
                      child: Center(
                          child: Text(Button.btnLeft,
                              style:
                                  TextStyle(fontSize: 17, color: Colors.blue))),
                    ),
                  ),
                  Container(width: 20, child: Center(child: Text('|'))),
                  InkWell(
                    onTap: () {
                      dragProvider.addVeritcalWood(2);
                    },
                    child: Container(
                      decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.2),
                              spreadRadius: 3,
                              blurRadius: 3,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ],
                          color: Colors.grey.withOpacity(0.1),
                          border: new Border.all(
                            color: Colors.blue.withOpacity(0.3),
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(5))),
                      width: 40,
                      child: Center(
                          child: Text(
                        Button.btnRight,
                        style: TextStyle(fontSize: 17, color: Colors.blue),
                      )),
                    ),
                  ),
                ],
              )
            : Container(),
        SizedBox(
          height: 20,
          width: 1,
        ),
      ],
    );
  }

  Widget _getRightMenu() {
    void _onLoading() {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return dialogLoading();
        },
      );
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        RaisedButton(
          color: Colors.blue,
          child: Text(
            '注意書き',
            style: TextStyle(color: Colors.white),
          ),
          onPressed: () async {
            String saveOrPrint = await DialogUtil.showDialogNote(context);
          },
        ),
        Divider(),
        RaisedButton(
          color: Colors.blue,
          onPressed: () async {
            String saveOrPrint = await DialogUtil.showDialogChoosePdf(context);
            if (saveOrPrint == "-1") return;
            _onLoading();

            await drawProvider.capturePng();
            await dragProvider.capturePng();
            final ttf = pw.Font.ttf(SizeConfig.font);
            final pdfDoc = buildPdf(
                context,
                // format,
                recordProvider.tempRecords,
                ttf,
                dragProvider.imageOfDrag,
                drawProvider.imageOfDraw,
                drawProvider.infor,
                recordProvider.numOfIndividuals,
                widget.hasBothType);
            if (saveOrPrint == "1") {
              final DateTime now = DateTime.now();
              final DateFormat formatter = DateFormat('yyyyMMddhhmmss');
              String formatted = formatter.format(now);
              Directory tempDir = await getTemporaryDirectory();
              String tempPath = tempDir.path;
              Record r = recordProvider.tempRecords[0];
              String fileName = r.scheduled_shipping_date +
                  "_" +
                  r.tmp_vehicle_number +
                  "_" +
                  r.flight_code +
                  "_" +
                  formatted;
              fileName = "${tempPath}/${fileName}.pdf";
              print(fileName);
              final file = File(fileName);

              await file.writeAsBytes(pdfDoc);
              await UploadFileHelper().uploadFile(fileName);
              Toast.show(Messages.SAVE_SUCCESS, context,
                  duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
            }
            if (saveOrPrint == "0") {
              Printing.layoutPdf(
                onLayout: (PdfPageFormat format) {
                  return pdfDoc;
                },
              );
            }
            Navigator.pop(context);
          },
          child: Container(
            width: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(
                  Icons.print,
                  color: Colors.white,
                ),
                Text(Button.btnPrint, style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
        ),
        RaisedButton(
          color: Colors.blue,
          onPressed: () async {
            bool undo = dragProvider.undoBox.length > 0;
            bool undoDraw = drawProvider.toUndo.length > 0;
            bool hadClear = drawProvider.hadClear;
            bool hadSave = drawProvider.hadSave;
            bool inforChanged = drawProvider.checkChangedInfo();
            bool addVerticalWoodChanged =
                dragProvider.checkVerticalWoodChanged();
            if ((undo ||
                    undoDraw ||
                    inforChanged ||
                    addVerticalWoodChanged ||
                    hadClear) &&
                !hadSave) {
              int confirm = await DialogUtil.showDialogConfirm(
                  context, Messages.RESET_BACK);
              if (confirm == 0) return;
            }

            dragProvider.reset();
            drawProvider.reset();

            Navigator.of(context).pushReplacement(PageRouteBuilder(
                pageBuilder: (context, animation, anotherAnimation) {
                  return ViewScreen2(
                    order: widget.order,
                    hasBothType: widget.hasBothType,
                  );
                },
                transitionDuration: Duration(milliseconds: 1000),
                transitionsBuilder:
                    (context, animation, anotherAnimation, child) {
                  animation =
                      CurvedAnimation(curve: Curves.easeIn, parent: animation);
                  return Align(
                      child: FadeTransition(
                    opacity: animation,
                    child: child,
                  ));
                }));
          },
          child: Container(
            width: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Icon(
                  Icons.keyboard_backspace,
                  color: Colors.white,
                ),
                Text(Button.btnBack, style: TextStyle(color: Colors.white)),
              ],
            ),
          ),
        )
      ],
    );
  }
}
