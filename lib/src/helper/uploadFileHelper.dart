import 'dart:convert';

import 'package:HirohataCargo/src/repo/uploadFileApi.dart';



class UploadFileHelper {
  final api = UploadFileApi();

  Future<bool> uploadFile(String imagePath) async {
    final apiResult = await api.uploadPdf(imagePath);
    // final data = jsonDecode(apiResult);
    // if (data != null) {
    //   return true;
    // }
    return true;
  }
}
